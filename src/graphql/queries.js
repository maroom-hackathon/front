import { gql } from '@apollo/client'

export const SEARCH = gql`
  query {
    search(input: { type: DATASET, query: "covid", start: 0, count: 10 }) {
      start
      count
      total
      searchResults {
        entity {
          urn
          type
          ...on Dataset {
              name
          }
        }
      }
    }
  }
`