const RouteAuthTypes = {
  loggedIn: "loggedIn",
  loggedOut: "loggedOut",
  free: "free",
}

export default RouteAuthTypes