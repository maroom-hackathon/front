import {makeAutoObservable} from "mobx";

class CartStore {
  rootStore

  cart

  constructor(rootStore) {
    this.rootStore = rootStore
    this.cart = JSON.parse(localStorage.getItem("cart")) || []
    makeAutoObservable(this, {rootStore: false})
  }

  addToCart = (dataset) => {
    this.cart = [...this.cart, dataset]
    localStorage.setItem("cart", JSON.stringify(this.cart))
  }

  removeFromCart = (id) => {
    this.cart = this.cart.filter((dataset) => dataset.id !== id)
    localStorage.setItem("cart", JSON.stringify(this.cart))
  }

  clearCart = () => {
    this.cart = []
    localStorage.removeItem("cart")
  }
}

export default CartStore