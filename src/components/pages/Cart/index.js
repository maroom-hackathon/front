import Page from "../../common/Page";
import {Box, Grid, makeStyles} from "@material-ui/core";
import CategoriesTree from "../../common/CategoriesTree";
import Datasets from "../../common/Datasets";
import Configuration from "../../common/Configuration";

const useStyles = makeStyles(({
  container: {
    background: "#F3F4F6",
    minHeight: "calc(100vh - 90px)"
  }
}))

const CartPage = () => {
  const classes = useStyles()

  return (
    <Page>
      <Box pt={3} pr={3} pl={3} className={classes.container}>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <CategoriesTree />
          </Grid>
          <Grid item xs={9}>
            <Configuration />
          </Grid>
        </Grid>
      </Box>
    </Page>
  )
}

export default CartPage