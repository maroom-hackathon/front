import {makeStyles, MenuItem, Select} from "@material-ui/core";
import {useState} from "react";

const useStyles = makeStyles({
  input: {
    background: "#FFF"
  }
})

const FilterInput = ({options, defaultValue, onChange, label}) => {
  const classes = useStyles()
  const [value, setValue] = useState(defaultValue)

  const handleChange = (e) => {
    setValue(e.target.value)
    if (onChange) {
      onChange(e)
    }
  }

  return (
    <Select
      fullWidth
      variant="outlined"
      value={value}
      label={label}
      onChange={handleChange}
      classes={{
       root: classes.input
      }}
    >
      {options?.map((option) => (
        <MenuItem value={option.value}>{option.label}</MenuItem>
      ))}
    </Select>
  )
}

FilterInput.defaultProps = {
  defaultValue: "",
}

export default FilterInput