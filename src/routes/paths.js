const Routes = {
  main: "/",
  signIn: "/signin",
  select: "/select",
  select_scor: "/select_scor",
  select_meta: "/select_meta",
  cart: "/cart",
  in_dev: "/in_dev",
}

export default Routes
