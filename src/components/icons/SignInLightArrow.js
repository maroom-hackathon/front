const SignInLightArrowIcon = ({ width, height }) => {
  return (
    <svg width={width} height={height} viewBox="0 0 379 375" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M167.493 -14.5H1.21446L211.653 194.145L212.011 194.5L211.653 194.855L1.21445 403.5H167.493L378.29 194.5L167.493 -14.5Z" fill="#90CAF9" stroke="#3F51B5"/>
    </svg>
  )
}

SignInLightArrowIcon.defaultProps = {
  width: 354,
  height: 350
}

export default SignInLightArrowIcon