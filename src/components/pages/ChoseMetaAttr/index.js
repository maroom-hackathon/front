import * as React from 'react'
import { useHistory } from "react-router-dom";
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Page from "../../common/Page";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 500,
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}));

const metas = [
  { id: 0, title: 'passport_id' },
  { id: 1, title: 'phone_id' },
  { id: 2, title: 'debt_id' },
  { id: 3, title: 'reg_region_id' },
  { id: 4, title: 'birthday_date' },
];

const ChoseMetaAttributes = () => {
  const [value, setValue] = React.useState([]);

  let history = useHistory();

  const onScorClick = () => {
    history.push("/cart");
  }

  return (
    <Page>
      <Container>
        <Box mt={5} mb={3}>
          <Grid container spacing={3} alignContent='center' justify='center'>
            <Grid container item xs={12} justify='center' alignContent='center'>
              <Typography variant='h2'>
                Поиск метатегов
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <Autocomplete
                  multiple
                  id="tags-standard"
                  options={metas}
                  getOptionLabel={(option) => option.title}
                  onChange={(event, newValue) => {
                    setValue([...newValue]);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="standard"
                      label="Выберите метатеги"
                      placeholder="Введите параметр поиска"
                    />
                  )}
                />
            </Grid>
            <Grid item xs={12} container justify='center' alignContent='center'>
              <Button onClick={onScorClick} disabled={value.length === 0} variant='contained' color='primary' size='large'>
                Далее
              </Button>
            </Grid>
          </Grid>
        </Box>        
      </Container>
    </Page>
  )
}

export default ChoseMetaAttributes
