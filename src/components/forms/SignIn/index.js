import * as yup from "yup"
import { yupResolver } from '@hookform/resolvers/yup';
import {useForm} from "react-hook-form";
import {Box, Button, TextField, Typography} from "@material-ui/core";

const Schema = yup.object().shape({
  user_name: yup.string().required("Введите имя пользователя"),
  user_password: yup.string().required("Введите пароль")
})

const SignInForm = ({handleSubmit}) => {
  const form = useForm({
    resolver: yupResolver(Schema)
  })

  return (
    <form onSubmit={form.handleSubmit(handleSubmit)}>
      <Box mb={3}>
        <Typography variant="h4">
          Вход в сервис
        </Typography>
      </Box>

      <Box mb={2}>
        <TextField
          fullWidth
          variant="outlined"
          label="Имя пользователя"
          error={form.formState.errors.user_name?.message}
          helperText={form.formState.errors.user_name?.message}
          {...form.register("user_name")}
        />
      </Box>
      <Box mb={2}>
        <TextField
          fullWidth
          variant="outlined"
          label="Пароль"
          type="password"
          error={form.formState.errors.user_password?.message}
          helperText={form.formState.errors.user_password?.message}
          {...form.register("user_password")}
        />
      </Box>

      <Button
        size="large"
        variant="contained"
        color="primary"
        type="submit"
      >
        Войти
      </Button>
    </form>
  )
}

export default SignInForm