import {useState} from "react";
import {Box, Button, Grid, makeStyles, TextField} from "@material-ui/core";
import {observer} from "mobx-react";
import store from "../../../store"
import UnificationTableItem from "../UnificationTableItem";
import PlusIcon from "../../icons/Plus";
import ArrowBottom from "../../icons/ArrowBottom";
import {useForm} from "react-hook-form";
import SaveBlock from "../SaveBlock";

const useStyles = makeStyles({
  button: {
    color: '#60ABF2',
    textTransform: "none"
  }
})

const UnificationTab = () => {
  const classes = useStyles()
  const form = useForm()
  const {
    configurationStore: {filteredDataset}
  } = store
  const [tableCount, setTableCount] = useState([0])

  const addTable = () => {
    setTableCount((prev) => [...prev, prev.length])
  }

  const removeTable = (index) => {
    setTableCount((prev) => prev.filter((item) => item !== index))
  }

  return (
    <Grid item xs={6}>
      <Box mt={5}>
        {tableCount.map((key) => (
          <UnificationTableItem form={form} handleDelete={() => removeTable(key)} key={key} />
        ))}
        <Box mt={3}>
          <Grid container spacing={3}>
            <Grid item>
              <Button
                className={classes.button}
                startIcon={<PlusIcon />}
                endIcon={<ArrowBottom />}
                onClick={addTable}
              >
                Добавить выборку
              </Button>
            </Grid>
          </Grid>
        </Box>
        <Box mt={3}>
          <SaveBlock />
        </Box>
      </Box>
    </Grid>
  )
}

export default observer(UnificationTab)