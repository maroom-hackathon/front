import {Grid, IconButton, Typography} from "@material-ui/core";
import TrashIcon from "../../icons/Trash";
import FilterInput from "../../ui/FilterInput";
import {observer} from "mobx-react";
import store from "../../../store"
import {useState} from "react";


const UnificationTableItem = ({handleDelete, form}) => {
  const [datasetId, setDatasetId] = useState(null)
  const {configurationStore: {filteredDataset}} = store

  const handleChange = (e) => {
    setDatasetId(e.target.value)
  }

  return (
    <Grid item xs={12} container spacing={1} alignItems="center">
      <Grid item md={5} sm={12}>
        <FilterInput
          options={filteredDataset?.map((dataset) => ({value: dataset.id, label: dataset.name}))}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={2}>
        <Typography>
          по ключу
        </Typography>
      </Grid>
      <Grid item md={4} sm={12}>
        <FilterInput
          options={datasetId ? filteredDataset.find((dataset) => dataset.id === datasetId).columns.map((column) => ({value: column.name, label: column.name})) : []}
        />
      </Grid>
      <Grid item xs={1} container justifyContent="flex-end" alignItems="center">
        <IconButton onClick={handleDelete}>
          <TrashIcon />
        </IconButton>
      </Grid>
    </Grid>
  )
}

export default observer(UnificationTableItem)