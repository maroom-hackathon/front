import * as React from 'react'
import { useHistory } from "react-router-dom";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Page from "../../common/Page";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const InDevelopmentPage = () => {
  return (
    <Page>
      <Box mt={5} mp={3}>
        <Grid container spacing={3}>
          <Grid container item xs={12} justify='center'>
            <Typography variant="h5">
              Страница еще в разработке :)
            </Typography>
          </Grid>
          <Grid container item xs={12} justify='center'>
            <img src="https://c.tenor.com/F22BAOorxHQAAAAC/work.gif" />
          </Grid>
        </Grid>
      </Box>
    </Page>
  )
}

export default InDevelopmentPage
