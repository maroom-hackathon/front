import {Box, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {observer} from "mobx-react";
import store from "../../../store"
import Dataset from "../Dataset";
import {Pagination} from "@material-ui/lab";

const useStyles = makeStyles({
  paper: {
    height: "100%",
    position: "relative",
    paddingBottom: 80,
  },
  title: {
    fontWeight: 500
  },
  heading: {
    padding: "12px 24px 8px",
    borderBottom: "1px solid #C4C4C4"
  },
  searchQuery: {
    color: "#757575"
  },
  pagination: {
    position: "absolute",
    width: "100%",
    bottom: 24
  }
})

const Datasets = () => {
  const classes = useStyles()

  const {
    categoriesStore: { categories, categoriesPageCount }
  } = store

  return (
    <Paper variant="outlined" className={classes.paper}>
      <div className={classes.heading}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="subtitle2" className={classes.title}>
              Датасеты
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2" className={classes.searchQuery}>
              Результат поискового запроса 1 - 10
            </Typography>
          </Grid>
        </Grid>
      </div>
      {categories.map((category) => (
        <Dataset key={category.id} dataset={category} />
      ))}
      <Grid className={classes.pagination} container justifyContent="center" alignItems="flex-end">
        <Pagination count={categoriesPageCount} />
      </Grid>
    </Paper>
  )
}

export default observer(Datasets)