import Header from "../Header";
import {Container} from "@material-ui/core";

const Page = ({withContainer, children}) => {
  return (
    <div>
      <Header />
      {withContainer ? (
        <main>
          <Container>
            {children}
          </Container>
        </main>
      ) : (
        <main>{children}</main>
      )}
    </div>
  )
}

Page.defaultProps = {
  withContainer: false
}

export default Page