const CrossIcon = () => (
  <svg width="11" height="10" viewBox="0 0 11 10" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10.1663 1.27334L9.22634 0.333344L5.49967 4.06001L1.77301 0.333344L0.833008 1.27334L4.55967 5.00001L0.833008 8.72668L1.77301 9.66668L5.49967 5.94001L9.22634 9.66668L10.1663 8.72668L6.43967 5.00001L10.1663 1.27334Z" fill="black" fill-opacity="0.87"/>
  </svg>
)

export default CrossIcon