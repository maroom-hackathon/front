import {Box, Breadcrumbs, Grid, Typography} from "@material-ui/core";
import InputSearch from "../../ui/InputSearch";
import {Link} from "react-router-dom";

const DatasetHeader = ({routes}) => {
  return (
    <Box pl={3} pr={3}>
      <Grid container justifyContent="space-between" alignItems="center">
        <Grid item>
          <Grid container>
            <Grid item>
              <InputSearch />
            </Grid>
            <Grid item>
              <Breadcrumbs separator=">">
                {routes.map((route, index) => (
                  <Link key={index} to={route.link}>
                    <Typography>
                      {route.label}
                    </Typography>
                  </Link>
                ))}
              </Breadcrumbs>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}

export default DatasetHeader