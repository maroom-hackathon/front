import * as React from 'react'
import {Box, Grid, Typography} from "@material-ui/core";
import FilterInput from "../../ui/FilterInput";
import {useForm} from "react-hook-form";
import SaveBlock from "../SaveBlock";
import {observer} from "mobx-react";
import store from "../../../store"

const UploadTab = () => {
  const {configurationStore: {filteredDataset}} = store
  const form = useForm()

  return (
    <Box mt={3}>
      <Grid container alignItems="center" spacing={3}>
        <Grid item>
          <Typography>
            Выгрузить из
          </Typography>
        </Grid>
        <Grid item xs={3}>
          <FilterInput
            options={filteredDataset?.map((dataset) => ({value: dataset.name, label: dataset.name}))}
            form={form}
          />
        </Grid>
        <Grid item>
          <SaveBlock type='download' />
        </Grid>
      </Grid>
    </Box>
  )
}

export default observer(UploadTab)