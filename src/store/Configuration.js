import {makeAutoObservable} from "mobx";

class ConfigurationStore {
  rootStore

  datasets

  filteredDataset

  selectedDataset

  constructor(rootStore) {
    this.rootStore = rootStore
    this.selectedDataset = null
    this.filteredDataset = []
    this.datasets = [
      {
        name: "Паспорт",
        columns: [
          { type: "date", name: "Дата выдачи" }
        ]
      }
    ]
    makeAutoObservable(this, {rootStore: false})
  }

  setSelectedDataset = (dataset) => {
    this.selectedDataset = dataset
  }

  addFilteredDataset = (dataset) => {
    this.filteredDataset = [...this.filteredDataset, dataset]
  }

  removeFilteredDataset = (id) => {
    this.filteredDataset = this.filteredDataset.filter((dataset) => dataset.id !== id)
  }
}

export default ConfigurationStore