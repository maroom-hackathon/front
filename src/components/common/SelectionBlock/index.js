import {Box, Button, Grid, makeStyles} from "@material-ui/core";
import {observer} from "mobx-react";
import store from "../../../store"
import CrossIcon from "../../icons/Cross";

const useStyles = makeStyles({
  dataset: {
    borderRadius: 5,
    height: 21,
    background: "rgba(0, 0, 0, 0.04)"
  },
  wrapper: {
    // borderTop: "1px solid #C4C4C4",
    // borderBottom: "1px solid #C4C4C4",
    padding: "16px 0"
  }
})

const SelectionBlock = () => {
  const classes = useStyles()
  const {
    configurationStore: {filteredDataset, setSelectedDataset}
  } = store

  return (
    <Box className={classes.wrapper}>
      <Grid container alignItems="center" spacing={2}>
        {filteredDataset.map((dataset) => (
          <Grid item>
            <Button
              onClick={() => setSelectedDataset(dataset)}
              className={classes.dataset}
              endIcon={<CrossIcon />}
            >
              {dataset.name}
            </Button>
          </Grid>
        ))}
      </Grid>
    </Box>
  )
}

export default observer(SelectionBlock)