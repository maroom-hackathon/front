import { observer } from 'mobx-react'
import RouteAuthTypes from "./routes/auths"
import {Route, Switch} from "react-router-dom"
import LoggedInRoute from "./routes/LoggedInRoute"
import LoggedOutRoute from "./routes/LoggedOutRoute"
import ChoseFirstScreen from './components/pages/ChoseFirstScreen'
import SignInPage from './components/pages/SignIn'
import routes from "./routes"
import store from './store'


const renderRoute = (route) => {
  switch (route.auth) {
    case RouteAuthTypes.free:
      return (
        <Route path={route.path} key={route.id} exact>
          {route.content}
        </Route>
      )
    case RouteAuthTypes.loggedIn:
      return (
        <LoggedInRoute path={route.path} key={route.id}>
          {route.content}
        </LoggedInRoute>
      )
    case RouteAuthTypes.loggedOut:
      return (
        <LoggedOutRoute path={route.path} key={route.id}>
          {route.content}
        </LoggedOutRoute>
      )
    default:
      return null
  }
}

const App = () => {
  const { authStore: { isAuthed } } = store
  return (
    <div>
      <Switch>
        <Route exact path="/" >
          {isAuthed ? <ChoseFirstScreen /> : <SignInPage />}
        </Route>
        {routes.map((route) => renderRoute(route))}
      </Switch>
    </div>
  )
}

export default observer(App)