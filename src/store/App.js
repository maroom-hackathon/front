import {makeAutoObservable} from "mobx"

class AppStore {
  rootStore

  isLoading

  constructor(rootStore) {
    this.rootStore = rootStore
    this.isLoading = false
    makeAutoObservable(this, {rootStore: false})
  }

  addAuthToken = (token) => {
    localStorage.setItem("authToken", token)
  }

  removeAuthToken = () => {
    localStorage.removeItem("authToken")
  }

  get authToken() {
    return localStorage.getItem("authToken") || ""
  }
}

export default AppStore