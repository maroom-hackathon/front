import {Box, makeStyles, Paper, Tab, Tabs, Typography} from "@material-ui/core";
import {useState} from "react";
import TabPanel from "../TabPanel";
import UnificationTab from "../UnificationTab";
import CreateTableTab from "../CreateTableTab";
import UploadTab from "../UploadTab";
import ReviewTab from "../ReviewTab";
import SelectionBlock from "../SelectionBlock";

const useStyles = makeStyles({
  tab: {
    padding: 0,
    marginRight: 24,
    minWidth: 0
  },
  paper: {
    height: "calc(100vh - 128px)",
  }
})

const Configuration = () => {
  const classes = useStyles()
  const [value, setValue] = useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Paper variant="outlined" className={classes.paper}>
      <Box pt={3} pl={3} pr={3}>
        <Typography variant="h5">
          Выборки
        </Typography>
        <SelectionBlock />
        <Box mt={4}>
          <Tabs indicatorColor="primary" value={value} onChange={handleChange}>
            <Tab classes={{root: classes.tab}} label="Обзор" />
            <Tab classes={{root: classes.tab}} label="Создать" />
            <Tab classes={{root: classes.tab}} label="Объединить" />
            <Tab classes={{root: classes.tab}} label="Выгрузить" />
          </Tabs>
          <TabPanel value={value} index={0}>
            <ReviewTab />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <CreateTableTab />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <UnificationTab />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <UploadTab />
          </TabPanel>
        </Box>
      </Box>
    </Paper>
  )
}

export default Configuration