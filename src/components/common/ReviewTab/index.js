import {Box, Grid, Table, TableBody, TableCell, TableHead, TableRow, TextField, Typography} from "@material-ui/core";
import FilterInput from "../../ui/FilterInput";
import {useForm} from "react-hook-form";
import {observer} from "mobx-react";
import store from "../../../store"

const ReviewTab = () => {
  const {
    configurationStore: {selectedDataset}
  } = store
  const columns = [
    {type: "Телефон", name: "telefon_number", description: "Описание"}
  ]

  return (
    <Grid item xs={8}>
      <Box mt={3}>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <Typography>
              Информация по выборке
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>
              {selectedDataset?.name}
            </Typography>
          </Grid>
        </Grid>
        <Box mt={2}>
          <Typography>
            Описание для информации по выборке
          </Typography>
        </Box>
        <Box mt={2}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Тип
                </TableCell>
                <TableCell>
                  Название колонки
                </TableCell>
                <TableCell>
                  Описание колонки
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {selectedDataset?.columns.map((column, index) => (
                <TableRow key={index}>
                  <TableCell>
                    {column.type}
                  </TableCell>
                  <TableCell>
                    {column.name}
                  </TableCell>
                  <TableCell>
                    {column.description}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </Box>
    </Grid>
  )
}

export default observer(ReviewTab)