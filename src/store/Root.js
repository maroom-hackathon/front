import AppStore from "./App";
import AuthStore from "./Auth";
import CategoriesStore from "./Categories"
import CartStore from "./Cart";
import ConfigurationStore from "./Configuration";

class RootStore {

  appStore

  authStore

  categoriesStore

  cartStore

  configurationStore

  constructor() {
    this.appStore = new AppStore(this)
    this.authStore = new AuthStore(this)
    this.categoriesStore = new CategoriesStore(this)
    this.cartStore = new CartStore(this)
    this.configurationStore = new ConfigurationStore(this)
  }
}

export default RootStore
