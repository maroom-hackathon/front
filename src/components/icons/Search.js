const SearchIcon = ({width, height}) => (
  <svg width={width} height={height} viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M13.4476 11H12.6552L12.3744 10.73C13.3573 9.59 13.9491 8.11 13.9491 6.5C13.9491 2.91 11.0304 0 7.42964 0C3.8289 0 0.910187 2.91 0.910187 6.5C0.910187 10.09 3.8289 13 7.42964 13C9.04446 13 10.5289 12.41 11.6723 11.43L11.9431 11.71V12.5L16.9581 17.49L18.4525 16L13.4476 11ZM7.42964 11C4.93219 11 2.91617 8.99 2.91617 6.5C2.91617 4.01 4.93219 2 7.42964 2C9.9271 2 11.9431 4.01 11.9431 6.5C11.9431 8.99 9.9271 11 7.42964 11Z" fill="black" fillOpacity="0.54"/>
  </svg>
)

SearchIcon.defaultProps = {
  width: 19,
  height: 18
}

export default SearchIcon