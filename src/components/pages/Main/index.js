import { useEffect, useState } from 'react'
import {Box, Grid, makeStyles, TextField, Button, Container, Typography} from "@material-ui/core";
import store from "../../../store"
import { useQuery, gql } from "@apollo/client"
import { SEARCH } from '../../../graphql/queries'
import Page from "../../common/Page";
import Datasets from "../../common/Datasets";
import CategoriesTree from "../../common/CategoriesTree";

const useStyles = makeStyles(({
  container: {
    background: "#F3F4F6",
    minHeight: "calc(100vh - 90px)"
  }
}))

const MainPage = () => {
  const { error, loading, data } = useQuery(SEARCH)
  const classes = useStyles()
  const [customData, setData] = useState([])

  useEffect(() => {
    console.log(data)
    setData(data)
  }, [data])

  return (
    <Page>
      <Box pt={3} pr={3} pl={3} className={classes.container}>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <CategoriesTree />
          </Grid>
          <Grid item xs={9}>
            <Datasets />
          </Grid>
        </Grid>
      </Box>
    </Page>
  )
}

export default MainPage