import { makeAutoObservable } from "mobx"

class CategoriesStore {
  rootStore

  err

  searchQuery = ''

  categoriesPageCount


  constructor(rootStore) {
    this.rootStore = rootStore
    this.categoriesPageCount = 1
    makeAutoObservable(this, { rootStore: false })
  }

  updateSearchQuery = (val) => {
    this.searchQuery = val
  }

  get categoriesTree() {
    return {
      id: 'root',
      name: 'Parent',
      children: [
        {
          id: '1',
          name: 'Бюро кредитных историй',
          columns: [
            { name: "Размер кредита", type: "number" },
            { name: "Является ли банкротом", type: "bool" },
          ],
          children: []
        },
        {
          id: '2',
          name: '2GIS',
          columns: [
            { name: "Численность нас. пункта", type: "number" },
          ],
          children: []
        },
        {
          id: '3',
          name: 'МВД',
          children: [
            {
              id: '4',
              name: 'Паспорт',
              columns: [
                { name: "Дата выдачи", type: "date" },
              ],
              children: []
            },
            // {
            //   id: '5',
            //   name: 'Сводки',
            //   columns: [
            //     { name: "Размер кредита", type: "number" },
            //   ],
            //   children: []
            // },
          ],
        },
      ],
    };
  }

  get categories() {
    return [
      {
        id: '4',
        name: 'Child - 4',
        desc: "description",
        platform: "platform",
        dataset: "dataset",
        company: "company"
      },
      {
        id: '5',
        name: 'Child - 5',
        desc: "description",
        platform: "platform",
        dataset: "dataset",
        company: "company"
      },
      {
        id: '6',
        name: 'Child - 4',
        desc: "description",
        platform: "platform",
        dataset: "dataset",
        company: "company"
      },
      {
        id: '7',
        name: 'Child - 5',
        desc: "description",
        platform: "platform",
        dataset: "dataset",
        company: "company"
      },
    ]
  }
}

export default CategoriesStore
