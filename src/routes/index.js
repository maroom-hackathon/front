import RouteAuthTypes from "./auths"
import Routes from "./paths"
import SignInPage from "../components/pages/SignIn"
import MainPage from "../components/pages/Main"
import ChoseFirstScreen from '../components/pages/ChoseFirstScreen'
import ChoseMetaAttributes from '../components/pages/ChoseMetaAttr'
import ChoseScores from '../components/pages/ChoseCategories'
import CartPage from "../components/pages/Cart";
import InDevPage from "../components/pages/InDevelopment";

const routes = [
  {
    id: 1,
    path: Routes.signIn,
    content: <SignInPage/>,
    auth: RouteAuthTypes.loggedOut
  },
  {
    id: 3,
    path: Routes.select,
    content: <ChoseFirstScreen />,
    auth: RouteAuthTypes.loggedIn
  },
  {
    id: 4,
    path: Routes.select_scor,
    content: <ChoseScores />,
    auth: RouteAuthTypes.loggedIn
  },
  {
    id: 5,
    path: Routes.select_meta,
    content: <ChoseMetaAttributes />,
    auth: RouteAuthTypes.loggedIn
  },
  {
    id: 2,
    path: Routes.cart,
    content: <CartPage/>,
    auth: RouteAuthTypes.loggedIn
  },
  {
    id: 6,
    path: Routes.id_dev,
    content: <InDevPage/>,
    auth: RouteAuthTypes.loggedIn
  },
]

export default routes