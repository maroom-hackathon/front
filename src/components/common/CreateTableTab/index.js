import {Box, Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {useState} from "react";
import FilterTableItem from "../FilterTableItem";
import {useForm} from "react-hook-form";
import PlusIcon from "../../icons/Plus";
import ArrowBottom from "../../icons/ArrowBottom";
import SaveBlock from "../SaveBlock";
import {observer} from "mobx-react";
import store from "../../../store"

const useStyles = makeStyles({
  input: {
    background: "#FFF",
  },
  button: {
    color: '#60ABF2',
    textTransform: "none"
  }
})

const CreateTableTab = () => {
  const classes = useStyles()
  const {configurationStore: {selectedDataset}} = store
  const [tableFilters, setTableFilters] = useState([0])
  const form = useForm()

  const addTableFilter = () => {
    setTableFilters((prev) => [...prev, prev.length])
  }

  const removeTableFilter = (index) => {
    setTableFilters((prev) => prev.filter((item) => item !== index))
  }

  return (
    <Box mt={3}>
      <Grid item xs={8} container spacing={3} alignItems="center">
        <Grid item xs={2}>
          <Typography>
            Из
          </Typography>
        </Grid>
        <Grid item xs={10}>
          <Typography>
            {selectedDataset?.name}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          {tableFilters.map((item) => (
            <FilterTableItem
              columns={selectedDataset?.columns}
              form={form}
              variant={item === 0 ? "first" : (item === 1 ? "second" : "other")}
              handleDelete={() => removeTableFilter(item)}
            />
          ))}
        </Grid>
        <Grid item xs={12}>
          <Box mt={3}>
            <Button
              className={classes.button}
              startIcon={<PlusIcon />}
              endIcon={<ArrowBottom />}
              onClick={addTableFilter}
            >
              Добавить фильтр
            </Button>
          </Box>
        </Grid>
        <Grid item xs={12}>
          <Box mt={3}>
            <SaveBlock />
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}

export default observer(CreateTableTab)