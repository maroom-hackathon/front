import {Redirect, Route} from "react-router-dom"
import {observer} from "mobx-react"
import store from "../store"

const LoggedInRoute = ({children, path}) => {
  const { isAuthed } = store.authStore

  if (isAuthed) {
    return (
      <Route path={path} exact strict>
        {children}
      </Route>
    )
  }

  return <Redirect to="/signin" />
}

export default observer(LoggedInRoute)