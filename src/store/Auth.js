import {makeAutoObservable} from "mobx"

class AuthStore {
  rootStore

  err

  isAuthed

  isLoading

  constructor(rootStore) {
    this.rootStore = rootStore
    this.isAuthed = !!rootStore.appStore.authToken
    this.isLoading = false
    makeAutoObservable(this, {rootStore: false})
  }

  signIn = async (data) => {
    const token = `${data.user_name}_${data.user_password}`
    this.isLoading = true
    this.rootStore.appStore.addAuthToken(token)
    this.isAuthed = true
  }

  logout = () => {
    this.isAuthed = false
  }
}

export default AuthStore