const ArrowBottomIcon = ({width, height}) => (
  <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M0.885 0.11499L-3.86846e-08 0.99999L5 5.99999L10 0.99999L9.115 0.11499L5 4.22999L0.885 0.11499Z" fill="#60ABF2"/>
  </svg>
)

ArrowBottomIcon.defaultProps = {
  width: 12,
  height: 8
}

export default ArrowBottomIcon