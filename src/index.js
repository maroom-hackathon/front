import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from "react-router-dom"
import { Provider } from "mobx-react"
import App from "./App"
import store from "./store"
import './index.css'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
  HttpLink,
  from
} from "@apollo/client";
import { onError } from '@apollo/client/link/error'

const errorLink = onError(({ graphqlErrors }) => {
  if (graphqlErrors) {
    graphqlErrors.map(({ message, location, path}) => {
      console.log(message, location, path)
    })
  }
})
const link = from([
  errorLink,
  new HttpLink({ uri: 'http://hackathon-maroom.tech:8000/api/v0/gql_proxy' })
])

const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <Provider {...store}>
          <Router>
            <App />
          </Router>
      </Provider>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

