import SignInForm from "../../forms/SignIn";
import {Box, Container, Grid, makeStyles} from "@material-ui/core";
import {observer} from "mobx-react";
import store from "../../../store"
import SignInLightArrow from "../../icons/SignInLightArrow";

const useStyles = makeStyles((theme) => ({
  container: {
    // background: "#e3f2fd",
    height: "100vh",
    position: "relative",
    overflow: "hidden",
  },
  icon: {
    height: "100%",
    position: "absolute",
    top: 0,
    left: "40%",
    "& svg": {
      height: "100vh",
      width: "auto",
    },
    "@media(max-width: 600px)": {
      left: "20%"
    }
  },
}))

const SignInPage = () => {
  const classes = useStyles()

  const {
    authStore: { signIn }
  } = store
  
  const handleSubmit = async (data) => {
    await signIn(data)
  }

  return (
    <Grid container alignItems="center" className={classes.container}>
      <Container>
        <Grid container>
          <Grid  lg={4} md={6} sm={8} xs={12}>
            <Box mt={2}>
              <SignInForm handleSubmit={handleSubmit} />
            </Box>
          </Grid>
        </Grid>
      </Container>
      <div className={classes.icon}>
        <SignInLightArrow />
      </div>
    </Grid>
  )
}

export default observer(SignInPage)