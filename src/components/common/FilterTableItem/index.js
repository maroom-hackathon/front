import {Grid, IconButton, TextField, Typography} from "@material-ui/core";
import FilterInput from "../../ui/FilterInput";
import TrashIcon from "../../icons/Trash";
import {useState} from "react";

const numberType = [
  {value: "Больше", label: "Больше"},
  {value: "Меньше", label: "Меньше"},
  {value: "Равно", label: "Равно"},
]

const boolType = [
  {value: "Да", label: "Да"},
  {value: "Нет", label: "Нет"},
]

const dateType = [
  {value: "От", label: "От"},
  {value: "До", label: "До"},
]

const selectType = {
  number: numberType,
  bool: boolType,
  date: dateType
}


const FilterTableItem = ({variant, form, handleDelete, columns}) => {
  const [type, setType] = useState("number")

  const handleChange = (e) => {
    setType(e.target.value)
  }

  return (
    <Grid container spacing={1} alignItems="center">
      {variant === "first" || variant === "other" ? (
        <Grid item xs={2}>
          <Typography>
            {variant === "first" ? "Где" : `И`}
          </Typography>
        </Grid>
      ) : (
        <Grid item xs={2}>
          <FilterInput defaultValue="and" options={[
            {value: "and", label: "И"},
            {value: "or", label: "Или"}
          ]}/>
        </Grid>
      )}
      <Grid item xs={3}>
        <FilterInput
          defaultValue="and"
          onChange={handleChange}
          options={columns?.map((column) => ({value: column.type, label: column.name}))}
        />
      </Grid>
      <Grid item xs={3}>
        <FilterInput defaultValue="and" options={selectType[type]}/>
      </Grid>
      {type !== 'bool' && (
        <Grid item xs={3}>
          <TextField variant="outlined"/>
        </Grid>
      )}
      <Grid item xs={1} container justifyContent="flex-end" alignItems="center">
        <IconButton onClick={handleDelete}>
          <TrashIcon />
        </IconButton>
      </Grid>
    </Grid>
  )
}

export default FilterTableItem