import * as React from 'react'
import store from '../../../store'
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import { observer } from 'mobx-react'
import {Box, makeStyles, Paper, Typography} from "@material-ui/core";
import InputSearch from "../../ui/InputSearch";

const useStyles = makeStyles({
  tree: {
    padding: "12px"
  },
  heading: {
    padding: "13px 16px",
    borderBottom: "1px solid #C4C4C4"
  },
  title: {
    fontWeight: 500
  },
  children: {
    background: "#F3F4F6"
  },
  parent: {
    background: "#E5E7EB"
  },
  treeItem: {
    paddingTop: 6,
    paddingBottom: 6
  },
  paper: {
    height: "calc(100vh - 194px)",
    overflow: "auto",
    scrollbarWidth: "2px",
    overflowY: "scroll",
    "&::-webkit-scrollbar": {
      width: "2px",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#C4C4C4"
    }
  }
})

const CategoriesTree = () => {
  const classes = useStyles()
  const {
    categoriesStore: {categoriesTree},
    configurationStore: {setSelectedDataset}
  } = store

  const handleClick = (node) => {
    if (node.columns) {
      setSelectedDataset(node)
    }
  }
  
  const renderTree = (nodes) => (
    <TreeItem
      key={nodes.id}
      nodeId={nodes.id}
      label={nodes.name}
      className={classes.treeItem}
      onClick={() => handleClick(nodes)}
    >
      {Array.isArray(nodes.children)
        ? nodes.children.map((node) => renderTree(node))
        : null}
    </TreeItem>
  );
  
  return (
    <div>
      <Box mb={3}>
        <InputSearch />
      </Box>
      <Paper variant="outlined" className={classes.paper}>
        <div className={classes.heading}>
          <Typography variant="subtitle2" className={classes.title}>
            Датасеты
          </Typography>
        </div>
        <div className={classes.tree}>
          <TreeView
            aria-label="rich object"
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpanded={['root']}
            defaultExpandIcon={<ChevronRightIcon />}
            sx={{ height: 110, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
          >
            {renderTree(categoriesTree)}
          </TreeView>
        </div>
      </Paper>
    </div>
  )
}

export default observer(CategoriesTree)
