import {
  AppBar,
  Box,
  Button,
  Grid,
  IconButton,
  makeStyles,
  Menu,
  MenuItem,
  Toolbar,
  Typography
} from "@material-ui/core";
import VTBIcon from "../../icons/VTB";
import ArrowBottom from "../../icons/ArrowBottom";
import {Link} from "react-router-dom";
import store from "../../../store"
import {observer} from "mobx-react";
import CartIcon from "../../icons/Cart";
import {useState} from "react";

const useStyles = makeStyles({
  header: {
    background: "#FFF",
    height: 90,
    padding: "8px 0",
    justifyContent: "center",
    boxShadow: "none",
    borderBottom: "1px solid #C4C4C4",
    "@media(max-width: 600px)": {
      height: 48
    }
  },
  link: {
    fontWeight: 500,
    color: "#3f51b5",
    fontSize: 16,
    textTransform: "none"
  },
  a: {
    textDecoration: "none"
  }
})

const Header = () => {
  const classes = useStyles()
  const {
    cartStore: {cart},
    authStore: {logout}
  } = store
  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <AppBar position="sticky" className={classes.header}>
      <Toolbar>
        <Grid item xs={3}>
          <IconButton
            size="small"
            edge="start"
          >
            <VTBIcon />
          </IconButton>
        </Grid>
        <Grid container alignItems="center" justifyContent="center" spacing={3}>
          <Grid item>
            <Link to="/cart" className={classes.a}>
              <Typography className={classes.link}>
                Выборки
              </Typography>
            </Link>
          </Grid>
          <Grid item>
            <Link to="/" className={classes.a}>
              <Typography className={classes.link}>
                Начало
              </Typography>
            </Link>
          </Grid>
        </Grid>
        <Grid container alignItems="center" justifyContent="flex-end">
          <Grid item>
            <Box pr={6}>
              {/* <Link to="/cart" className={classes.a}>
                <Button size="large" startIcon={<CartIcon />}>
                  <Typography className={classes.link}>
                    Корзина {cart.length > 0 ? cart.length : ""}
                  </Typography>
                </Button>
              </Link> */}
            </Box>
          </Grid>
          <Grid item>
            <IconButton
              size="medium"
              edge="start"
              sx={{ mr: 2 }}
              onClick={handleClick}
            >
              <ArrowBottom />
            </IconButton>
            <Menu
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={logout}>Выйти</MenuItem>
            </Menu>
          </Grid>
          <Grid item>
            <IconButton
              size="small"
              edge="end"
              sx={{ mr: 2 }}
            >
              <img width={40} height={40} src="/images/Avatar.png" alt="avatar" />
            </IconButton>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  )
}

export default observer(Header)