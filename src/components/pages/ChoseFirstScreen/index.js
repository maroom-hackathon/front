import * as React from 'react'
import { useHistory } from "react-router-dom";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Page from "../../common/Page";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const ChoseFirstScreen = () => {
  let history = useHistory();

  const onScorClick = () => {
    history.push("/select_scor");
  }

  const onMetaClick = () => {
    history.push("/select_meta");
  }

  return (
    <Page>
      <Container>
        <Box mt={5} mb={3}>
        <Grid container spacing={3} alignContent='center' justify='center'>
          <Grid item xs={12}>
            <Typography variant="h3" component="h2">
              Выберите тип генерации
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Card onClick={onScorClick}>
              <CardContent>
                  <Typography variant="h5" component="div">
                    Генерация по скорам
                  </Typography>
                  <br />
                  <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    Описание генерации по скорам
                  </Typography>
                  <br />
                </CardContent>
            </Card>
          </Grid>
          <Grid item xs={6}>
            <Card onClick={onMetaClick}>
              <CardContent>
                <Typography variant="h5" component="div">
                  Генерация по метотегам
                </Typography>
                <br />
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                  Описание генерации по метотегам
                </Typography>
                  <br />
              </CardContent>
              </Card>
          </Grid>
        </Grid>
        </Box>        
      </Container>
    </Page>
  )
}

export default ChoseFirstScreen
