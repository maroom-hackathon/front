import {
  Box,
  Grid,
  makeStyles,
  Typography,
  Button,
  IconButton,
  Accordion,
  AccordionSummary,
  AccordionDetails
} from "@material-ui/core";
import {observer} from "mobx-react";
import store from "../../../store"
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import PlusIcon from "../../icons/Plus";
import MinusIcon from "../../icons/Minus";

const useStyles = makeStyles({
  dataset: {
    borderBottom: "1px solid #C4C4C4"
  },
  name: {
    fontWeight: 500,
    color: '#225094'
  },
  desc: {
    color: "#9A9EA5"
  },
  info: {
    color: "#666666",
    lineHeight: "20px"
  },
  borderRight: {
    borderRight: "1px solid #C4C4C4"
  },
  accordionSummary: {
    width: "100%",
    margin: 0,
    display: "block",
    "&$expanded": {
      margin: 0
    }
  },
  accordion: {
    padding: 0
  },
  accordionContainer: {
    boxShadow: "none",
    margin: "0px !important",
    "&::before": {
      content: "none"
    }
  },
  expanded: {
    margin: 0
  },
  details: {
    background: "#FAFAFA",
    padding: 24,
    borderBottom: "1px solid #C4C4C4"
  }
})

const Dataset = ({ dataset }) => {
  const classes = useStyles()
  const [isAddToCart, setAddToCart] = useState(false)
  const {
    cartStore: { addToCart, cart, removeFromCart }
  } = store


  useEffect(() => {
    if (cart.find((item) => item.id === dataset.id)) {
      setAddToCart(true)
    } else {
      setAddToCart(false)
    }
  }, [cart])

  const handleClick = () => {
    if (cart.find((item) => item.id === dataset.id)) {
      removeFromCart(dataset.id)
    } else {
      addToCart(dataset)
    }
  }

  return (
    <Accordion classes={{root: classes.accordionContainer}}>
      <AccordionSummary
        classes={{
          content: classes.accordionSummary,
          root: classes.accordion,
          expanded: classes.expanded
        }}
      >
        <Box pr={3} className={classes.dataset}>
          <Grid container justifyContent="space-between" alignItems="center">
            <Grid item>
              <Box pl={3} pr={3} pt={3} pb={3} >
                <Grid container>
                  <Grid item className={classes.borderRight}>
                    <Box pr={2}>
                      <Typography variant="subtitle1" className={classes.info}>
                        {dataset.platform}
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item className={classes.borderRight}>
                    <Box pr={2} pl={2}>
                      <Typography variant="subtitle1" className={classes.info}>
                        {dataset.dataset}
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box pl={2}>
                      <Typography variant="subtitle1" className={classes.info}>
                        {dataset.company}
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
                <Link to={`/datasets/${dataset.id}`}>
                  <Box mt={2}>
                    <Typography variant="h6" className={classes.name}>
                      {dataset.name}
                    </Typography>
                    <Typography variant="subtitle1" className={classes.desc}>
                      {dataset.desc}
                    </Typography>
                  </Box>
                </Link>
              </Box>
            </Grid>
            <Grid item>
              <Grid container>
                {/*<Grid item>*/}
                {/*  <IconButton onClick={handleClick}>*/}
                {/*    {isAddToCart ? <MinusIcon/> : <PlusIcon/>}*/}
                {/*  </IconButton>*/}
                {/*</Grid>*/}
                <Grid item>
                  <IconButton onClick={handleClick}>
                    {isAddToCart ? <MinusIcon/> : <PlusIcon/>}
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </AccordionSummary>
      <AccordionDetails classes={{root: classes.details}}>
        details
      </AccordionDetails>
    </Accordion>
  )
}

export default observer(Dataset)