import * as React from 'react'
import {Button, Grid, TextField} from "@material-ui/core";
import store from "../../../store"
import {useForm} from "react-hook-form";

const SaveBlock = ({ onChange, type }) => {
  const [fileName, setFileName] = React.useState('')
  const form = useForm()
  const {configurationStore: {addFilteredDataset, filteredDataset}} = store

  const handleSubmit = (data) => {
    addFilteredDataset({
      id: filteredDataset.length + 10,
      name: data.name,
      columns: [
        {name: "Тест", type: "number"}
      ]
    })
    form.reset()
  }

  return (
    <form onSubmit={form.handleSubmit(handleSubmit)}>
      <Grid container justifyContent="flex-end" alignItems="center" l spacing={3}>
        <Grid item>
          <TextField
            variant="outlined"
            fullWidth
            placeholder="Название файла"
            onChnage={(e, val) => setFileName(val)}
            {...form.register("name")}
          />
        </Grid>
        <Grid item>
          {type === 'download' ?
            <a href={form.watch('name') + ".json"} download>
              <Button type="button" color="primary" variant="contained">
                Выгрузить
              </Button>
            </a>
          : <Button type="submit" color="primary" variant="contained">
                Сохранить
              </Button>}
        </Grid>
      </Grid>
    </form>
  )
}

export default SaveBlock