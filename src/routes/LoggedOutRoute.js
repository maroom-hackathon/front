import {Redirect, Route} from "react-router-dom";
import {observer} from "mobx-react";
import store from "../store"

const LoggedOutRoute = ({children, path}) => {
  const {isAuthed} = store.authStore
  if (!isAuthed) {
    return (
      <Route path={path} exact strict>
        {children}
      </Route>
    )
  }

  return <Redirect to="/" />
}

export default observer(LoggedOutRoute)