import * as React from 'react'
import { useHistory } from "react-router-dom";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Page from "../../common/Page";

const options = {
    id: 0,
    name: 'По типу',
    next: 1,
    options: [
      {
        id: 1,
        name: 'Финансовые сервисы',
        options: [],
      },
      {
        id: 2,
        name: 'Аналитические сервисы',
        options: [],
      },
      {
        id: 6,
        name: 'Маркетинг',
        options: [
          {
            id: 5,
            name: 'Выделение ядра аудитории',
            options: []
          },
          {
            id: 8,
            name: 'Создание look-like аудитории',
            options: [
               {
                id: 51,
                name: 'Контрагенты',
                options: []
              },
              {
                id: 52,
                name: 'Сотрудники',
                options: []
              },
              {
                id: 53,
                name: 'Клиенты',
                options: []
              },
            ]
          },
        ],
      }
    ]
  }

const ChoseCategoriesPage = () => {
  let history = useHistory()
  const [currentId, setCurrentId] = React.useState(0)
  const [oldValues, setOldValues] = React.useState([])
  const [inputAmounts, setInputAmounts] = React.useState([0])
  const [step, setStep] = React.useState(0)

  const currentOption = (() => {
    let finded;
    const iter = node => {
        const { options } = node;
        if (node.id === currentId) {
            finded = node;
        }
        if (node.options) {
          node.options.map((el) => iter(el));
        }
    }
    iter(options);
    return finded;
  })()

  const handleButton = () => {
    history.push('/in_dev')
  }

  const handleInputChange = (event, value, index) => {
    console.log(value, step, index)
    if (value) {
      if (index < step) {
        setOldValues(prev => prev.filter((el, curIndex) => curIndex <= index))
        setInputAmounts(prev => prev.filter((curIndex) => curIndex <= index))
      } else {
        setOldValues(prev => [...prev, { ...currentOption, selected: value }])
        if (value.options && value.options.length > 0) {
          console.log(value, 'should not updated')
          setInputAmounts(prev => [...prev, prev.length])
        }
        setStep(prev => prev + 1)
      }
      setCurrentId(value.id)
    }
  }
  
  return (
    <Page>
      <Container>
        <Box mt={5} mb={3}>
          <Grid container spacing={3} alignContent='center' justify='center'>
          <Grid item xs={12} container alignContent='center' justify='center'>
            <Typography variant='h3'>
              Выберите скоры
            </Typography>
          </Grid>
          <Grid item xs={8} container alignContent='center' justify='center'>
            {inputAmounts.map((current, index) => 
              (<Grid key={index} item xs={12} container alignContent='center' justify='center'>
                <Box mt={3} mb={3}>
                  <Autocomplete
                    key={index}
                    id={index}
                    defaultValue={step === current ? '' : oldValues[current].selected}
                    value={step === current ? '' : oldValues[current].selected}
                    options={step === current ? currentOption.options : oldValues[current].options}
                    getOptionLabel={(option) => option.name}
                    style={{ width: 300 }}
                    onChange={(e, value) => handleInputChange(e, value, current)}
                    renderInput={(params) =>
                      <TextField
                        {...params}
                        label={step === current ? currentOption.name : oldValues[current].name}
                        variant="outlined"
                        defaultValue={step === current ? '' : oldValues[current].selected}
                        value={step === current ? '' : oldValues[current].selected}
                      />
                    }
                  />
                </Box>
              </Grid>)
            )}
          </Grid>
          <Grid item xs={12} container justify='center' alignContent='center'>
            <Button disabled={step === 0} variant="contained" color="primary" onClick={handleButton}>
              Далее
            </Button>
          </Grid>
        </Grid>
        </Box>
      </Container>
    </Page>
  )
}

export default ChoseCategoriesPage
