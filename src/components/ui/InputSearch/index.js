import {InputAdornment, makeStyles, TextField} from "@material-ui/core";
import SearchIcon from "../../icons/Search";
import * as React from "react";
import {observer} from "mobx-react";
import store from "../../../store"

const useStyles = makeStyles({
  root: {
    height: 43,
    background: "#FFF",
  },
})

const InputSearch = () => {
  const classes = useStyles()
  const { categoriesStore: {
    updateSearchQuery,
  }} = store

  const inputHandle = (event) => updateSearchQuery(event.target.value)

  return (
    <TextField
      fullWidth
      variant="outlined"
      onChange={inputHandle}
      placeholder="Поисковый запрос"
      InputProps={{
        classes: {
          root: classes.root
        },
        endAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        )
      }}
    />
  )
}

export default observer(InputSearch)